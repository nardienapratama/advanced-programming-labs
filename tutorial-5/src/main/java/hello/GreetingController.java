package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingController {

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name = "name", required = true)
                                       String name, Model model) {
        model.addAttribute("name", name);
        return "greeting";
    }

    @GetMapping("/cv")
    public String cv(@RequestParam(name = "visitor", required = false)
                                        String name, Model model) {
        // name="visitor" is from URL variable; 2nd line: name is the actual name in URL
        if (name == null) {
            model.addAttribute("visitor", "This is my CV"); //"visitor" is var in html file
        } else {
            String desc = name + ", I hope you can hire me.";
            model.addAttribute("visitor", desc);
        }

        return "cv";

    }

}
