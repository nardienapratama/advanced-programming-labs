/*
 * Copyright 2012-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hello;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertFalse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = GreetingController.class)
public class ApplicationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void test()
    {
        Application.main(new String[]{
                "--spring.main.web-environment=false",
                "--spring.autoconfigure.exclude=false",
                // Override any other environment properties according to your needs
        });
    }

    @Test
    public void homePage() throws Exception {
        // N.B. jsoup can be useful for asserting HTML content
        mockMvc.perform(get("/index.html"))
                .andExpect(content().string(containsString("Get your greeting")));
    }

    @Test
    public void homePageNegative() throws Exception {
        MvcResult result = mockMvc.perform(get("/index.html"))
                .andExpect(content().string(containsString("Get your greeting"))).andReturn();
        String content = result.getResponse().getContentAsString();

        assertFalse(content.isEmpty());
    }

    @Test
    public void greetingWithUser() throws Exception {
        mockMvc.perform(get("/greeting").param("name", "Greg"))
                .andExpect(content().string(containsString("Hello, Greg!")));
    }

    @Test
    public void greetingWithUserNegative() throws Exception {
        mockMvc.perform(get("/greeting").param("name", ""))
                .andExpect(content().string(containsString("Hello, ")));

    }

    @Test
    public void cvCustomUser() throws Exception {
        mockMvc.perform(get("/cv").param("visitor", "Anne"))
                .andExpect(content().string(containsString("Anne, I hope you can hire me.")));
    }

    @Test
    public void cvCustomUserNegative() throws Exception {
        mockMvc.perform(get("/cv").param("visitor", ""))
                .andExpect(content().string(containsString(", I hope you can hire me.")));
    }

    @Test
    public void cvUser() throws Exception {
        mockMvc.perform(get("/cv"))
                .andExpect(content().string(containsString("This is my CV")));
    }

}
