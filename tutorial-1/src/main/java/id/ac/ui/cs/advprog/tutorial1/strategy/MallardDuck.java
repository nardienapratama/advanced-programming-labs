package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MallardDuck extends Duck {
    // TODO Complete me!
    public MallardDuck(){
        setQuackBehavior(new Quack());        // uses the quack class to handle its quack
        setFlyBehavior(new FlyWithWings());   // uses FlyWithWings as its behavior type
    }

    // the quackBehavior and flyBehavior instance variables are inherited from the Duck class

    public void display(){
        System.out.println("I'm a real Mallard duck");
    }


}
