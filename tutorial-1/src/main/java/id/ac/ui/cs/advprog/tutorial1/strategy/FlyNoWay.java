package id.ac.ui.cs.advprog.tutorial1.strategy;

public class FlyNoWay implements FlyBehavior {
    // TODO Complete me!
    public FlyNoWay(){
        
    }
    @Override
    public void fly(){
        System.out.println("I can't fly");
    }
}
