package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class MediumThicknessCrustDough implements Dough {
    public String toString() {
        return "Crust with Medium Thickness";
    }
}