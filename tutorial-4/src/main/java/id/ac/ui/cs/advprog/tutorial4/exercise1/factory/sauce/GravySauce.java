package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class GravySauce implements Sauce {
    public String toString() {
        return "Gravy sauce with some mushrooms";
    }
}