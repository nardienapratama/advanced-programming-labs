package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class AllDoughTest {

    private Dough dough;

    @Test
    public void testName() {
        dough = new MediumThicknessCrustDough();
        assertEquals("Crust with Medium Thickness", dough.toString());

        dough = new ThickCrustDough();
        assertEquals("ThickCrust style extra thick crust dough", dough.toString());

        dough = new ThinCrustDough();
        assertEquals("Thin Crust Dough", dough.toString());

    }
}
