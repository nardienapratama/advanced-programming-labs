package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class AllSauceTest {

    private Sauce sauce;

    @Test
    public void testName() {
        sauce = new GravySauce();
        assertEquals("Gravy sauce with some mushrooms", sauce.toString());

        sauce = new MarinaraSauce();
        assertEquals("Marinara Sauce", sauce.toString());

        sauce = new PlumTomatoSauce();
        assertEquals("Tomato sauce with plum tomatoes", sauce.toString());

    }
}
