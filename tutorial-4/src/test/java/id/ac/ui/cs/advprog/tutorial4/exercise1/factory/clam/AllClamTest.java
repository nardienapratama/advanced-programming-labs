package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class AllClamTest {

    private Clams clam;

    @Test
    public void testName() {
        clam = new FreshClams();
        assertEquals("Fresh Clams from Long Island Sound", clam.toString());

        clam = new FrozenClams();
        assertEquals("Frozen Clams from Chesapeake Bay", clam.toString());

        clam = new HardClams();
        assertEquals("Hard Clams from Depok Baru", clam.toString());
    }
}
