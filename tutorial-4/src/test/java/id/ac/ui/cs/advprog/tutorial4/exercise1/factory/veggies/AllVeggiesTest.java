package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class AllVeggiesTest {

    private Veggies veggie;

    @Test
    public void testName() {
        veggie = new BlackOlives();
        assertEquals("Black Olives", veggie.toString());

        veggie = new Cabbage();
        assertEquals("Cabbage", veggie.toString());

        veggie = new Eggplant();
        assertEquals("Eggplant", veggie.toString());

        veggie = new Garlic();
        assertEquals("Garlic", veggie.toString());

        veggie = new Mushroom();
        assertEquals("Mushrooms", veggie.toString());

        veggie = new Onion();
        assertEquals("Onion", veggie.toString());

        veggie = new RedPepper();
        assertEquals("Red Pepper", veggie.toString());

        veggie = new Spinach();
        assertEquals("Spinach", veggie.toString());


    }
}
