package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class AllCheeseTest {

    private Cheese cheese;

    @Test
    public void testName() {
        cheese = new CamembertCheese();
        assertEquals(cheese.toString(), "Camembert Cheese");

        cheese = new MozzarellaCheese();
        assertEquals(cheese.toString(), "Shredded Mozzarella");

        cheese = new ParmesanCheese();
        assertEquals(cheese.toString(), "Shredded Parmesan");

        cheese = new ReggianoCheese();
        assertEquals(cheese.toString(), "Reggiano Cheese");
    }
}
