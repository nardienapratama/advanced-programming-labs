package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;


public class PizzaTest {

    private Pizza ethanOrder;
    private Pizza annaOrder;
    private Pizza bethOrder;
    private Pizza ethanSecOrder;
    private Pizza annaSecOrder;
    private Pizza bethSecOrder;

    private String ethanPizza;
    private String annaPizza;
    private String bethPizza;

    @Before
    public void setUp() {
        PizzaStore newYork = new NewYorkPizzaStore();

        ethanOrder = newYork.orderPizza("cheese");

        annaOrder = newYork.orderPizza("clam");

        bethOrder = newYork.orderPizza("veggie");

        PizzaStore depok = new DepokPizzaStore();

        ethanSecOrder = depok.orderPizza("cheese");

        annaSecOrder = depok.orderPizza("clam");

        bethSecOrder = depok.orderPizza("veggie");
    }

    @Test
    public void testPizzaName() {
        ethanPizza = ethanOrder.getName();
        assertEquals("New York Style Cheese Pizza", ethanPizza);

        annaPizza = annaOrder.getName();
        assertEquals("New York Style Clam Pizza", annaPizza);

        bethPizza = bethOrder.getName();
        assertEquals("New York Style Veggie Pizza", bethPizza);

        ethanPizza = ethanSecOrder.getName();
        assertEquals("Depok Style Cheese Pizza", ethanPizza);

        annaPizza = annaSecOrder.getName();
        assertEquals("Depok Style Clam Pizza", annaPizza);

        bethPizza = bethSecOrder.getName();
        assertEquals("Depok Style Veggie Pizza", bethPizza);

    }


    @Test
    public void testPizzaToString() {

        ethanPizza = ethanOrder.toString();
        assertEquals("---- New York Style Cheese Pizza ----\n"
                + "Thin Crust Dough\n"
                + "Marinara Sauce\n"
                + "Reggiano Cheese\n", ethanPizza);

        annaPizza = annaOrder.toString();
        assertEquals("---- New York Style Clam Pizza ----\n"
                + "Thin Crust Dough\n"
                + "Marinara Sauce\n"
                + "Reggiano Cheese\n"
                + "Fresh Clams from Long Island Sound\n", annaPizza);

        bethPizza = bethOrder.toString();
        assertEquals("---- New York Style Veggie Pizza ----\n"
                + "Thin Crust Dough\n"
                + "Marinara Sauce\n"
                + "Reggiano Cheese\n"
                + "Garlic, Onion, Mushrooms, Red Pepper\n", bethPizza);

        ethanPizza = ethanSecOrder.toString();
        assertEquals("---- Depok Style Cheese Pizza ----\n"
                + "ThickCrust style extra thick crust dough\n"
                + "Gravy sauce with some mushrooms\n"
                + "Shredded Mozzarella\n", ethanPizza);

        annaPizza = annaSecOrder.toString();
        assertEquals("---- Depok Style Clam Pizza ----\n"
                + "ThickCrust style extra thick crust dough\n"
                + "Gravy sauce with some mushrooms\n"
                + "Shredded Mozzarella\n"
                + "Hard Clams from Depok Baru\n", annaPizza);

        bethPizza = bethSecOrder.toString();
        assertEquals("---- Depok Style Veggie Pizza ----\n"
                + "ThickCrust style extra thick crust dough\n"
                + "Gravy sauce with some mushrooms\n"
                + "Shredded Mozzarella\n"
                + "Spinach, Cabbage, Eggplant, Black Olives\n", bethPizza);


    }

}
