package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;


public class AdditionalCompany {

    private static Company company;
    private static BackendProgrammer anna;
    private static FrontendProgrammer beck;
    private static NetworkExpert greg;
    private static SecurityExpert cory;
    private static UiUxDesigner sam;



    public static void main(String[] args) {
        company = new Company();

        anna = new BackendProgrammer("Anna", 50000000.00);
        company.addEmployee(anna);      // adds anna to list of employees

        beck = new FrontendProgrammer("Beck", 40000000.00);
        company.addEmployee(beck);

        greg = new NetworkExpert("Greg", 70000000.00);
        company.addEmployee(greg);

        cory = new SecurityExpert("Cory", 10000000.00);
        company.addEmployee(cory);

        sam = new UiUxDesigner("Sam", 10000000.00);
        company.addEmployee(sam);

    }
}