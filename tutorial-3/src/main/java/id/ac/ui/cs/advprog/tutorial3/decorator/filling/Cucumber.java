package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Cucumber extends Food {
    Food food;

    public Cucumber(Food food) {
        //TODO Implement
        this.food = food;
    }

    @Override
    public String getDescription() {
        //TODO Implement
        this.description = food.getDescription() + ", adding cucumber";
        return this.description;
    }

    @Override
    public double cost() {
        //TODO Implement
        return 0.4 + food.cost();
    }
}
