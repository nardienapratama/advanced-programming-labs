import id.ac.ui.cs.advprog.tutorial3.decorator.Food;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.CrustySandwich;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.NoCrustSandwich;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.ThickBunBurger;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.ThinBunBurger;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.BeefMeat;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Cheese;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.ChickenMeat;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.ChiliSauce;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Cucumber;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Lettuce;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Tomato;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.TomatoSauce;



public class FoodFilling {
    public static void main(String[] args) {
        Food food = new ThickBunBurger();
        food = new BeefMeat(food);
        food = new Cheese(food);
        food = new Cucumber(food);
        food = new Lettuce(food);
        food = new ChiliSauce(food);

        Food newfood = new ThinBunBurger();
        newfood = new Tomato(newfood);
        newfood = new Cucumber(newfood);
        newfood = new Lettuce(newfood);

        Food newestfood = new CrustySandwich();
        newestfood = new BeefMeat(newestfood);
        newestfood = new ChickenMeat(newestfood);
        newestfood = new Tomato(newestfood);
        newestfood = new ChiliSauce(newestfood);

        Food nocrustfood = new NoCrustSandwich();
        nocrustfood = new BeefMeat(nocrustfood);
        nocrustfood = new Cheese(nocrustfood);
        nocrustfood = new ChickenMeat(nocrustfood);
        nocrustfood = new ChiliSauce(nocrustfood);
        nocrustfood = new Cucumber(nocrustfood);
        nocrustfood = new Lettuce(nocrustfood);
        nocrustfood = new Tomato(nocrustfood);
        nocrustfood = new TomatoSauce(nocrustfood);



    }
}