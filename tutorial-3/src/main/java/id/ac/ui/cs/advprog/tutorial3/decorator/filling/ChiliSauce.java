package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ChiliSauce extends Food {
    Food food;

    public ChiliSauce(Food food) {
        //TODO Implement
        this.food = food;
    }

    @Override
    public String getDescription() {
        //TODO Implement
        this.description = food.getDescription() + ", adding chili sauce";
        return this.description;
    }

    @Override
    public double cost() {
        //TODO Implement
        return 0.3 + food.cost();
    }
}
