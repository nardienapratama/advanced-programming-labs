package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class BeefMeat extends Food {
    Food food;

    public BeefMeat(Food food) {
        //TODO Implement
        this.food = food;
    }

    @Override
    public String getDescription() {
        //TODO Implement
        this.description = food.getDescription() + ", adding beef meat";
        return this.description;
    }

    @Override
    public double cost() {
        //TODO Implement
        return 6.0 + food.cost();   
    }
}
