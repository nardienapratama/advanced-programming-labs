package id.ac.ui.cs.advprog.tutorial3.decorator.bread;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ThickBunBurger extends Food {
    public ThickBunBurger() {
        //TODO Implement
        description = "Thick Bun Burger";
    }

    @Override
    public double cost() {
        //TODO Implement
        double cost = 2.5;
        return cost;
    }
}
