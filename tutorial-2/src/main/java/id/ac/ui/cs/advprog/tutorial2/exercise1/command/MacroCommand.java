package id.ac.ui.cs.advprog.tutorial2.exercise1.command;

import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;

public class MacroCommand implements Command {

    private List<Command> commands;


    public MacroCommand(Command[] commands) {
        this.commands = Arrays.asList(commands);
    }

    @Override
    public void execute() {
        // TODO Complete me!
        commands.forEach(Command::execute);
        
    }

    @Override
    public void undo() {
        // TODO Complete me!
        // for (int i = 0; i < commands.size();i++) {
        //     commandlist.add(i);
        // }
        new LinkedList<>(commands)
                    .descendingIterator()
                    .forEachRemaining(Command::undo);
    }
}
